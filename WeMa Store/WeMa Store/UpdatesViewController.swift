//
//  UpdatesViewController.swift
//  WeMa Store
//
//  Created by Niels van den Bout on 24-06-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift
import Alamofire
import Haneke
//import Crashlytics

class UpdatesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var label: UILabel!
    
    var installed = [AppsArray]()
    var notInstalled = [AppsArray]()
    var updates = [AppsArray]()
    var buttonTitle: String!
    var refreshControl: UIRefreshControl!
    var apps: [AppsArray]?
    var IPAInstallUrl: String!
    var loginName: String!
    var app = AppsArray()
    var appSections = [AppSectionData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews() {
        self.navigationController?.clearNavBar()
        if NSUserDefaults.standardUserDefaults().stringForKey("name") != nil {
            loginName = NSUserDefaults.standardUserDefaults().stringForKey("name")!
        }
        tableView.dataSource = self
        tableView.delegate = self
        self.tableView.contentInset = UIEdgeInsetsMake(64,0,49,0);
        navigationItem.backBarButtonItem?.title = "Updates"
        self.refreshControl = UIRefreshControl()
        self.refreshControl.backgroundColor = UIColor(red: 0, green: 0, blue: 47/255, alpha: 1.0)
        self.refreshControl.addTarget(self, action: #selector(StoreViewController.downloadData), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(self.refreshControl)
        self.title = "Updates"
        downloadData()
        tableView.tableFooterView = UIView()
    }
    
    func close() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func downloadingAlert() {
        let attributedMessage = NSAttributedString(string: "Wanneer u op downloaden heeft gedrukt, gaat u naar het homescreen om de installatie te bekijken. Als u op annuleren heeft gedrukt, kunt u dit bericht negeren.", attributes: [
            NSFontAttributeName : UIFont.systemFontOfSize(14),
            NSForegroundColorAttributeName : UIColor.lightGrayColor()
            ])
        
        let alertController = UIAlertController(title: "Downloaden", message: "", preferredStyle: .Alert)
        alertController.setValue(setLogOffButton("Downloaden"), forKey: "attributedTitle")
        alertController.setValue(attributedMessage, forKey: "attributedMessage")
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (UIAlertAction) in
        }))
        presentViewController(alertController, animated: true, completion: nil)
        showVisualEffect(alertController)
    }
    
    func showVisualEffect(superView: UIAlertController) {
        if let visualEffectView = superView.view.searchVisualEffectsSubview()
        {
            visualEffectView.effect = UIBlurEffect(style: .Dark)
        }
    }
    
    func downloadData() {
        if WeMaStoreAPI.sharedInstance.installed.count < 1 && WeMaStoreAPI.sharedInstance.notInstalled.count < 1 && WeMaStoreAPI.sharedInstance.updates.count < 1 {
        let company_code = NSUserDefaults.standardUserDefaults().stringForKey("company_code")!
        if Reachability.isConnectedToNetwork() {
            WeMaStoreAPI.sharedInstance.getApps(loginName, owner_code: company_code) { (responseObject, error) in
                self.clearArrays()
                if let response = responseObject?.newApps {
                    for newApp in response {
                        self.checkIfInstalled(newApp)
                    }
                }
                if let response = responseObject?.installedApps {
                    for newApp in response {
                        self.checkIfInstalled(newApp)
                    }
                }
                if let response = responseObject?.updateableApps {
                    for newApp in response {
                        self.checkForUpdate(newApp)
                    }
                }
                
                self.setData()
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
                self.tabBarItem.badgeValue = "\(self.updates.count)"
                 let application = UIApplication.sharedApplication()
                application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: UIUserNotificationType.Badge, categories: nil ))
                application.applicationIconBadgeNumber = self.updates.count
                if self.updates.count + self.installed.count == 0 {
                    self.label.hidden = false
                } else {
                    self.label.hidden = true
                }
                UpdatesCount.sharedInstance.count = self.updates.count
            }
        } else {
            showAlertView("Geen internet", message: "U heeft op dit moment geen internetverbinding.")
        }
        } else {
            self.clearArrays()
            
            installed = WeMaStoreAPI.sharedInstance.installed
            notInstalled = WeMaStoreAPI.sharedInstance.notInstalled
            updates = WeMaStoreAPI.sharedInstance.updates
            
            self.setData()
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
            self.tabBarItem.badgeValue = "\(self.updates.count)"
            let application = UIApplication.sharedApplication()
            application.registerUserNotificationSettings(UIUserNotificationSettings(forTypes: UIUserNotificationType.Badge, categories: nil ))
            application.applicationIconBadgeNumber = self.updates.count
            if self.updates.count + self.installed.count == 0 {
                self.label.hidden = false
            } else {
                self.label.hidden = true
            }
            UpdatesCount.sharedInstance.count = self.updates.count
        }
    }
    
    func setData() {
        let installedSection = AppSectionData(title: "Geïnstalleerd", data: installed)
        let updatesSection = AppSectionData(title: "Beschikbare Updates", data: updates)
        appSections.append(updatesSection)
        appSections.append(installedSection)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        if tabBarController == nil {
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: #selector(close))
        }
    }
    
    func clearArrays() {
        installed.removeAll()
        notInstalled.removeAll()
        updates.removeAll()
        appSections.removeAll()
    }
    
    func showActions() {
        let alertController = UIAlertController(title: "Uitloggen", message: "Weet u zeker dat u wilt uitloggen?", preferredStyle: .Alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (UIAlertAction) in
            if #available(iOS 9, *) {
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                let navBar = self.storyboard?.instantiateViewControllerWithIdentifier("loginVC")
                self.navigationController?.popToRootViewControllerAnimated(true)
                appDelegate.window?.rootViewController = navBar
            }
            NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "success")
        }))
        
        alertController.addAction(UIAlertAction(title: "Annuleren", style: .Cancel, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appSections[section].numberOfItems
    }
    
    @IBAction func cellButtonPressed(sender: UIButton) {
        if sender.tag == 1 {
            installNewApp(sender)
        } else if sender.tag == 2 {
            openInstalledApp(sender)
        } else if sender.tag == 0 {
            installNewApp(sender)
        }
    }
    
    func openInstalledApp(sender: AnyObject) {
        if let button = sender as? UIButton {
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            if let superview = button.superview {
                if let cell = superview.superview as? AppsTableViewCell {
                    if let appURL = NSURL(string: "\(cell.deepLink)://") {
                        let canOpen = UIApplication.sharedApplication().canOpenURL(appURL)
                        if canOpen {
                            UIApplication.sharedApplication().openURL(appURL)
                        } else {
                        }
                    }
                }
            }
        }
    }
    
    func installNewApp(sender: AnyObject) {
        var downloadUrl: String! = ""
        var app: AppsArray
        var section: Int
        if let button = sender as? UIButton {
            if let superview = button.superview {
                if let cell = superview.superview as? AppsTableViewCell {
                    let position: CGPoint = sender.convertPoint(CGPointZero, toView: self.tableView)
                    if let indexPath = self.tableView.indexPathForRowAtPoint(position) {
                        section = indexPath.section
                        downloadUrl = cell.downloadUrl!
                        if section == 0 {
                            app = updates[cell.tag]
                        } else {
                            app = notInstalled[cell.tag]
                        }
                        WeMaStoreAPI.sharedInstance.appInstallInfo(loginName, packageName: app.package_name, version: app.latestVersion!.version_no, action: "installed") {}
                    }
                }
            }
        }
        let url = NSURL(string: "itms-services://?action=download-manifest&url=https://www.iappministrator.com/market/apks/\(downloadUrl!)")!
        let request = NSURLRequest(URL: url)
        webView.loadRequest(request)
        downloadingAlert()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! AppsTableViewCell
        cell.backgroundColor = cell.contentView.backgroundColor
        let appIcon = appSections[indexPath.section][indexPath.row].icon
        let appDeepLink = appSections[indexPath.section][indexPath.row].package_name
        cell.versionLabel.text = "Versie: \(appSections[indexPath.section][indexPath.row].latestVersion!.version_name)"
        cell.buildLabel.text = "Build: \(appSections[indexPath.section][indexPath.row].latestVersion!.version_no)"
        cell.downloadUrl = appSections[indexPath.section][indexPath.row].latestVersion!.download_url!
        cell.nameLabel.text = appSections[indexPath.section][indexPath.row].name
        let url = "http://www.iappministrator.com/market/icons/\(appIcon)"
        setupImage(cell.appImage, url: url)
        cell.deepLink = appDeepLink
        cell.tag = indexPath.row
        setupButtons(cell, indexPath: indexPath)
        return cell
    }
    
    func setupImage(superView: UIImageView, url: String) {
        superView.hnk_setImageFromURL(NSURL(fileURLWithPath: url))
        let URL = NSURL(string: url)!
        let cache = Shared.imageCache
        let fetcher = NetworkFetcher<UIImage>(URL: URL)
        cache.fetch(fetcher: fetcher).onSuccess { image in
            superView.image = image
        }
        cache.fetch(fetcher: fetcher).onFailure({ (error) in
            superView.image = UIImage(named: "iOS_7_Icon_Template")
        })
    }
    
    func setupButtons(cell: AppsTableViewCell, indexPath: NSIndexPath) {
        if appSections[indexPath.section].title == "Beschikbare Updates" {
            cell.deleteButton.setTitleWithOutAnimation("WERK BIJ")
            cell.buttonWidthConstant.constant = 85
            cell.deleteButton.tag = 1
        } else if appSections[indexPath.section].title == "Geïnstalleerd" {
            cell.deleteButton.setTitleWithOutAnimation("OPEN")
            cell.buttonWidthConstant.constant = 50
            cell.deleteButton.tag = 2
        } else if appSections[indexPath.section].title == "Nieuwe Apps" {
            cell.deleteButton.setTitleWithOutAnimation("DOWNLOAD")
            cell.buttonWidthConstant.constant = 95
            cell.deleteButton.tag = 0
        }
        cell.deleteButton.layer.borderColor = Color.sharedInstance.blueColor.CGColor
        cell.deleteButton.layer.borderWidth = 1
        cell.deleteButton.layer.cornerRadius = 4
        cell.deleteButton.clipsToBounds = true
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return appSections.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return appSections[section].title
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if appSections[section].data.count < 1 {
            return 0
        } else {
            return 28
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRectMake(0, 0, tableView.frame.size.width, 30))
        headerView.backgroundColor = UIColor(rgba: "#050035")
        let headerLabel = UILabel(frame: CGRectMake(10, 5, tableView.frame.width, 28))
        headerLabel.textColor = UIColor.whiteColor()
        headerLabel.font = UIFont.systemFontOfSize(14, weight: 0.3)
        headerLabel.text = self.tableView(self.tableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        return headerView
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        app = appSections[indexPath.section][indexPath.row]
        if appSections[indexPath.section].title == "Beschikbare Updates" {
            buttonTitle = "WERK BIJ"
        } else if appSections[indexPath.section].title == "Geïnstalleerd" {
            buttonTitle = "OPEN"
        } else if appSections[indexPath.section].title == "Nieuwe Apps" {
            buttonTitle = "DOWNLOAD"
        }
        performSegueWithIdentifier("showApp", sender: self)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showApp" {
            let destinationVC = segue.destinationViewController as! InstallViewController
            destinationVC.buttonTitle = buttonTitle
            destinationVC.app = app
        }
    }
    
    func showAlertView(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Oke", style: UIAlertActionStyle.Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func checkIfInstalled(dataItem: AppsArray) -> Bool {
        if let appURL = NSURL(string: "\(dataItem.package_name)://") {
            let canOpen = UIApplication.sharedApplication().canOpenURL(appURL)
            if canOpen {
                installed.append(dataItem)
                return true
            } else {
                notInstalled.append(dataItem)
                return false
            }
        } else {
            notInstalled.append(dataItem)
            return false
        }
    }
    
    func setLogOffButton(title: String) -> NSAttributedString {
        let attributedString: NSAttributedString
        attributedString = NSAttributedString(string: title, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(16, weight: 0.4),NSForegroundColorAttributeName : Color.sharedInstance.blueColor])
        return attributedString
    }
    
    func checkForUpdate(dataItem: AppsArray) -> Bool {
        if let appURL = NSURL(string: "\(dataItem.package_name)://") {
            let canOpen = UIApplication.sharedApplication().canOpenURL(appURL)
            if canOpen {
                updates.append(dataItem)
                return true
            } else {
                notInstalled.append(dataItem)
                return false
            }
        } else {
            notInstalled.append(dataItem)
            return false
        }
    }
}

