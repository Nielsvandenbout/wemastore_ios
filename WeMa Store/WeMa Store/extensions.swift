//
//  extensions.swift
//  WeMa Store
//
//  Created by Niels on 19-05-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Set button title without animation.
extension UIButton {
    func setTitleWithOutAnimation(title: String?) {
        UIView.setAnimationsEnabled(false)
        
        setTitle(title, forState: .Normal)
        
        layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
}

struct SectionData {
    let title: String
    let data : [String]
    
    var numberOfItems: Int {
        return data.count
    }
    
    subscript(index: Int) -> String {
        return data[index]
    }
}

struct AppSectionData {
    let title: String
    let data: [AppsArray]
    
    var numberOfItems: Int {
        return data.count
    }
    
    subscript(index: Int) -> AppsArray {
        return data[index]
    }
}

extension SectionData {
    //  Putting a new init method here means we can
    //  keep the original, memberwise initaliser.
    init(title: String, data: String...) {
        self.title = title
        self.data  = data
    }
}

struct ScreenSize
{
    static let SCREEN_WIDTH         = UIScreen.mainScreen().bounds.size.width
    static let SCREEN_HEIGHT        = UIScreen.mainScreen().bounds.size.height
    static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType
{
    static let iPhone4s  = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
    static let iPhone5         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
    static let iPhone6        = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
    static let iPhone6p         = UIDevice.currentDevice().userInterfaceIdiom == .Phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
    static let iPad             = UIDevice.currentDevice().userInterfaceIdiom == .Pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}

// MARK: - show/ hide hairline navigationbar
extension UINavigationBar {
    
    func hideBottomHairline()
    {
        hairlineImageViewInNavigationBar(self)?.hidden = true
    }
    
    func showBottomHairline()
    {
        hairlineImageViewInNavigationBar(self)?.hidden = false
    }
    
    private func hairlineImageViewInNavigationBar(view: UIView) -> UIImageView?
    {
        if let imageView = view as? UIImageView where imageView.bounds.height <= 1
        {
            return imageView
        }
        
        for subview: UIView in view.subviews
        {
            if let imageView = hairlineImageViewInNavigationBar(subview)
            {
                return imageView
            }
        }
        
        return nil
    }
    
}

// Show or hide line on toolbar
extension UIToolbar
{
    
    func hideHairline()
    {
        hairlineImageViewInToolbar(self)?.hidden = true
    }
    
    func showHairline()
    {
        hairlineImageViewInToolbar(self)?.hidden = false
    }
    
    private func hairlineImageViewInToolbar(view: UIView) -> UIImageView?
    {
        if let imageView = view as? UIImageView where imageView.bounds.height <= 1
        {
            return imageView
        }
        
        for subview: UIView in view.subviews
        {
            if let imageView = hairlineImageViewInToolbar(subview)
            {
                return imageView
            }
        }
        
        return nil
    }
    
}

// MARK: - Show app version and appbuild
extension NSBundle {
    
    var releaseVersionNumber: String? {
        return self.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    var buildVersionNumber: String? {
        return self.infoDictionary?["CFBundleVersion"] as? String
    }
    
}

// MARK: - Make navigationbar dark
extension UINavigationController  {
//    func addBlurEffect() {
//        self.navigationBar.backgroundColor = UIColor.clearColor()
//        self.navigationBar.barTintColor = UIColor.clearColor()
//        var bounds = self.navigationBar.bounds as CGRect!
//        bounds.offsetInPlace(dx: 0.0, dy: -20.0)
//        bounds.size.height = bounds.height + 20.0
//        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Dark))
//        visualEffectView.frame = bounds
//        visualEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
//        self.navigationBar.addSubview(visualEffectView)
//        self.navigationBar.sendSubviewToBack(visualEffectView)
//        visualEffectView.userInteractionEnabled = false
//    }
    func clearNavBar() {
        self.navigationBar.backgroundColor = UIColor.clearColor()
        self.navigationBar.barTintColor = UIColor.clearColor()
    }
}

// MARK: - take elements of an array
extension Array {
    func takeElements(inout elementCount: Int) -> Array {
        if (elementCount > count) {
            elementCount = count
        }
        return Array(self[0..<elementCount])
    }
}

// MARK: - set count of updates for badges.
class UpdatesCount {
    class var sharedInstance: UpdatesCount {
        struct Singleton {
            static let instance = UpdatesCount()
        }
        return Singleton.instance
    }
    var count: Int!
}

// MARK: - set colors whole app.
class Color {
    class var sharedInstance: Color {
        struct Singleton {
            static let instance = Color()
        }
        return Singleton.instance
    }

    let defaultTintColor = UIColor(red: 0.0, green: 122/255, blue: 1.0, alpha: 1)
    let blueColor = UIColor(colorLiteralRed: 19/256, green: 158/256, blue: 234/256, alpha: 1.0)
    let orangeColor = UIColor(red: 255/255, green: 88/255, blue: 0.0, alpha: 1.0)
}



// MARK: - search for blur view in toolbars.
extension UIView
{
    func searchVisualEffectsSubview() -> UIVisualEffectView?
    {
        if let visualEffectView = self as? UIVisualEffectView
        {
            return visualEffectView
        }
        else
        {
            for subview in subviews
            {
                if let found = subview.searchVisualEffectsSubview()
                {
                    return found
                }
            }
        }
        
        return nil
    }
}
