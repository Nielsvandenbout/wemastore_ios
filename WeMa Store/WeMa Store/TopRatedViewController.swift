//
//  TopRatedViewController.swift
//  WeMa Store
//
//  Created by Niels van den Bout on 24-06-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift
import Alamofire
import Haneke

protocol TopRatedViewControllerDelegate {
    func showApp(app: AppsArray)
}


class TopRatedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, TopRatedViewControllerDelegate {
    
    @IBOutlet weak var wmSliderBackground: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var webView: UIWebView!
    
    class var sharedInstance: TopRatedViewController {
        struct Singleton {
            static let instance = TopRatedViewController()
        }
        return Singleton.instance
    }
    
    var app = AppsArray()
    var loginName: String!
    var buttonTitle: String!
    var refreshControl: UIRefreshControl!
    var apps: [AppsArray]?
    var appSections = [AppSectionData]()
    var wmImageSlider: WMImageSlider! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews() {
        if NSUserDefaults.standardUserDefaults().stringForKey("name") != nil {
            loginName = NSUserDefaults.standardUserDefaults().stringForKey("name")!
        }
        downloadData()
        self.navigationController?.clearNavBar()
        addWMSlider()
        tableView.dataSource = self
        tableView.delegate = self
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "list"), style: .Plain, target: self, action: #selector(showUpdates))
        self.refreshControl = UIRefreshControl()
        self.refreshControl.backgroundColor = UIColor(red: 0, green: 0, blue: 47/255, alpha: 1.0)
        self.refreshControl.addTarget(self, action: #selector(TopRatedViewController.reloadTableView), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(self.refreshControl)
        self.tableView.contentInset = UIEdgeInsetsMake(64,0,0,0)
    }
    
    func downloadData() {
        if WeMaStoreAPI.sharedInstance.installed.count < 1 && WeMaStoreAPI.sharedInstance.notInstalled.count < 1 && WeMaStoreAPI.sharedInstance.updates.count < 1 {
            let company_code = NSUserDefaults.standardUserDefaults().stringForKey("company_code")!
            WeMaStoreAPI.sharedInstance.getApps(loginName, owner_code: company_code) { (responseObject, error) in
                if responseObject != nil {
                    self.apps = [AppsArray]()
                    for item in responseObject!.newApps! {
                        self.apps?.append(item)
                    }
                    for item in responseObject!.updateableApps! {
                        self.apps?.append(item)
                    }
                    for item in responseObject!.installedApps! {
                        self.apps?.append(item)
                    }
                    if self.apps?.count == 1 {
                        self.tabBarController?.selectedIndex = 1
                    }
                }
            }
        } else {
            self.apps = [AppsArray]()
            for item in WeMaStoreAPI.sharedInstance.notInstalled {
                self.apps?.append(item)
            }
            for item in WeMaStoreAPI.sharedInstance.updates {
                self.apps?.append(item)
            }
            for item in WeMaStoreAPI.sharedInstance.installed {
                self.apps?.append(item)
            }
            if self.apps?.count == 1 {
                self.tabBarController?.selectedIndex = 1
            }
 
        }
    }
    
    func reloadTableView() {
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    func showUpdates() {
        performSegueWithIdentifier("showUpdates", sender: self)
    }
    
    func showApp(app: AppsArray) {
        self.app = app
        self.buttonTitle = "DOWNLOAD"
        performSegueWithIdentifier("showApp", sender: self)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func addWMSlider() {
        let images = ["heracles", "kleding", "autowereld", "fashiola", "Unknown"]
        wmImageSlider = {
            let wmImageSlider = WMImageSlider(images: images, imageSelectedCallback: { (index) -> () in
                let imageName = images[index].capitalizedString
                if self.apps != nil {
                    for item in self.apps! {
                        if item.name.containsString(imageName) {
                            self.app = item
                            self.buttonTitle = "DOWNLOAD"
                            self.performSegueWithIdentifier("showApp", sender: self)
                        }
                    }
                }
            })
            wmImageSlider.scrollDirectionLandscape = true
            return wmImageSlider
        }()
        wmImageSlider.frame.size.width = view.frame.size.width
        wmImageSlider.frame.size.height = wmSliderBackground.frame.size.height
        wmImageSlider.center = CGPoint(x: view.frame.size.width / 2, y: wmSliderBackground.frame.size.height / 2)
        wmSliderBackground.addSubview(wmImageSlider)
        wmSliderBackground.userInteractionEnabled = true
        wmSliderBackground.bringSubviewToFront(wmImageSlider)
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! TableViewCell
        cell.downloadData()
        cell.topRatedVCDelegate = self
        cell.backgroundColor = cell.contentView.backgroundColor
        if indexPath.row == 0 {
            cell.titleLabel.text = "De beste apps"
        } else if indexPath.row == 1 {
            cell.titleLabel.text = "Updates"
        } else if indexPath.row == 2 {
            cell.titleLabel.text = "Deze heeft u al geïnstalleerd"
        }
        cell.collectionView.reloadData()
        cell.tag = indexPath.row
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showApp" {
            let destinationVC = segue.destinationViewController as! InstallViewController
            destinationVC.title = ""//name
            destinationVC.buttonTitle = buttonTitle
            destinationVC.app = app
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        tableView.reloadData()
    }
}