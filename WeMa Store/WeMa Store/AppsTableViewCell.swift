//
//  AppsTableViewCell.swift
//  WeMa Store
//
//  Created by Niels on 13-05-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import UIKit

class AppsTableViewCell: UITableViewCell {

    @IBOutlet weak var appImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var buildLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var buttonWidthConstant: NSLayoutConstraint!
    var deepLink: String!
    var downloadUrl: String!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        appImage.layer.cornerRadius = 13
        appImage.layer.borderColor = UIColor.lightGrayColor().CGColor
        appImage.layer.borderWidth = 0.0
        appImage.clipsToBounds = true
    }



}
