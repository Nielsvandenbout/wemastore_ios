//
//  StoreViewController.swift
//  WeMa Store
//
//  Created by Niels on 13-05-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import UIKit
import UIColor_Hex_Swift
import Alamofire
import Haneke
import Crashlytics

class StoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIToolbarDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var waitView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var toolBar: UIToolbar!
    @IBOutlet weak var tableView: UITableView!
    
    var installed = [AppsArray]()
    var notInstalled = [AppsArray]()
    var updates = [AppsArray]()
    var selectedApp = AppsArray()
    var buttonTitle: String!
    var refreshControl: UIRefreshControl!
    var apps: [AppsArray]?
    var loginName: String!
    var type: String!
    var appSections = [AppSectionData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews() {
        toolBar.hidden = true
        waitView.hidden = false
        if NSUserDefaults.standardUserDefaults().stringForKey("name") != nil {
            loginName = NSUserDefaults.standardUserDefaults().stringForKey("name")!
        }
//        addBlurToToolbar()
        self.navigationController?.clearNavBar()
        title = " "
        self.toolBar.showHairline()
        segmentedControl.hidden = true
        self.tableView.contentInset = UIEdgeInsetsMake(108, 0, 49, 0)
        tableView.dataSource = self
        tableView.delegate = self
        tabBarController?.tabBar.hidden = true
        self.refreshControl = UIRefreshControl()
        self.refreshControl.backgroundColor = UIColor(red: 0, green: 0, blue: 47/255, alpha: 1.0)
        self.refreshControl.addTarget(self, action: #selector(StoreViewController.downloadData), forControlEvents: UIControlEvents.ValueChanged)
        self.tableView.addSubview(self.refreshControl)
        downloadData()
        tableView.tableFooterView = UIView()
    }
    
    func positionForBar(bar: UIBarPositioning) -> UIBarPosition {
        return UIBarPosition.TopAttached
    }
    
    @IBAction func valueChanged(sender: AnyObject) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            tableView.reloadData()
        case 1:
            tableView.reloadData()
        case 2:
            tableView.reloadData()
        default:
            debugPrint("impossible")
        }
    }
    
    func downloadingAlert() {
        let attributedMessage = NSAttributedString(string: "Wanneer u op downloaden heeft gedrukt, gaat u naar het homescreen om de installatie te bekijken. Als u op annuleren heeft gedrukt, kunt u dit bericht negeren.", attributes: [NSFontAttributeName : UIFont.systemFontOfSize(14),NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        let alertController = UIAlertController(title: "Downloaden", message: "", preferredStyle: .Alert)
        alertController.setValue(setLogOffButton("Downloaden"), forKey: "attributedTitle")
        alertController.setValue(attributedMessage, forKey: "attributedMessage")
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
        showVisualEffect(alertController)
    }
    
    func showVisualEffect(superView: UIAlertController) {
        if let visualEffectView = superView.view.searchVisualEffectsSubview()
        {
            visualEffectView.effect = UIBlurEffect(style: .Dark)
        }
    }
    
//    func addBlurToToolbar() {
//        let bounds = self.navigationController?.navigationBar.bounds as CGRect!
//        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Dark))
//        visualEffectView.frame = bounds
//        visualEffectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
//        self.toolBar.addSubview(visualEffectView)
//        self.toolBar.sendSubviewToBack(visualEffectView)
//        visualEffectView.userInteractionEnabled = false
//    }
    
    func downloadData() {
        let company_code = NSUserDefaults.standardUserDefaults().stringForKey("company_code")!
        loginName = NSUserDefaults.standardUserDefaults().stringForKey("name")!
        if Reachability.isConnectedToNetwork() {
            WeMaStoreAPI.sharedInstance.getApps(loginName, owner_code: company_code) { (responseObject, error) in
                self.clearArrays()
                if let response = responseObject?.installedApps {
                    for newApp in response {
                        self.checkIfInstalled(newApp)
                    }
                }
                if let response = responseObject?.updateableApps {
                    for newApp in response {
                        self.checkForUpdate(newApp)
                    }
                }
                self.setData()
                self.tableView.reloadData()
                self.refreshControl.endRefreshing()
                if self.updates.count + self.notInstalled.count + self.installed.count == 1 {
                    self.setButtonTitle()
                    self.performSegueWithIdentifier("showSingleApp", sender: self)
                } else {
                    self.setupRestOfViews()
                }
            }
        } else {
            showAlertView("Geen internet", message: "U heeft op dit moment geen internetverbinding.")
        }
    }
    
    func setupRestOfViews() {
        self.toolBar.delegate = self
        self.segmentedControl.tintColor = Color.sharedInstance.blueColor
        self.navigationItem.backBarButtonItem?.title = "WeMa Store"
        let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FixedSpace, target: nil, action: nil)
        negativeSpacer.width = 5
        let barButtonItem = UIBarButtonItem(title: "Uitloggen", style: .Plain, target: self, action: #selector(self.logoff))
        self.navigationItem.setLeftBarButtonItems([negativeSpacer, barButtonItem], animated: false)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "list"), style: .Plain, target: self, action: #selector(self.showUpdates))
        self.title = "WeMa Store"
        if self.updates.count > 0 {
            self.tabBarController?.tabBar.items?[3].badgeValue = "\(self.updates.count)"
        }
        self.waitView.hidden = true
        self.segmentedControl.hidden = false
        self.tabBarController?.tabBar.hidden = false
        self.toolBar.hidden = false
    }
    
    func setButtonTitle() {
        if self.updates.count == 1 {
            self.selectedApp = self.updates.first!
            self.buttonTitle = "WERK BIJ"
        } else if self.installed.count == 1 {
            self.selectedApp = self.installed.first!
            self.buttonTitle = "OPEN"
        } else if self.notInstalled.count == 1 {
            self.selectedApp = self.notInstalled.first!
            self.buttonTitle = "DOWNLOAD"
        }
    }
    
    func showAlertView(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Oke", style: UIAlertActionStyle.Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func setData() {
        WeMaStoreAPI.sharedInstance.installed = installed
        WeMaStoreAPI.sharedInstance.updates = updates
        WeMaStoreAPI.sharedInstance.notInstalled = notInstalled
        let installedSection = AppSectionData(title: "Geïnstalleerd", data: installed)
        let updatesSection = AppSectionData(title: "Beschikbare Updates", data: updates)
        let notInstalledSection = AppSectionData(title: "Nieuwe Apps", data: notInstalled)
        appSections.append(updatesSection)
        appSections.append(notInstalledSection)
        appSections.append(installedSection)
    }
    
    override func viewWillAppear(animated: Bool) {
        navigationController?.navigationBar.hideBottomHairline()
    }
    
    func clearArrays() {
        installed.removeAll()
        notInstalled.removeAll()
        updates.removeAll()
        appSections.removeAll()
    }
    
    func logoff() {
        let attributedString: NSAttributedString
        attributedString = NSAttributedString(string: "Uitloggen", attributes: [NSFontAttributeName : UIFont.systemFontOfSize(16, weight: 0.4), NSForegroundColorAttributeName : Color.sharedInstance.blueColor])
        let attributedMessage = NSAttributedString(string: "Weet u zeker dat u wilt uitloggen?", attributes: [
            NSFontAttributeName : UIFont.systemFontOfSize(14), NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .Alert)
        alertController.setValue(attributedString, forKey: "attributedTitle")
        alertController.setValue(attributedMessage, forKey: "attributedMessage")
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (UIAlertAction) in
            self.clearUserDefaults()
            PushNotificationManager.pushManager().unregisterForPushNotifications()
            if #available(iOS 9, *) {
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                let navBar = self.storyboard?.instantiateViewControllerWithIdentifier("loginVC")
                self.navigationController?.popToRootViewControllerAnimated(true)
                appDelegate.window?.rootViewController = navBar
            }
             let application = UIApplication.sharedApplication()
            application.applicationIconBadgeNumber = 0
        }))
        alertController.addAction(UIAlertAction(title: "Annuleren", style: .Cancel, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
        showVisualEffect(alertController)
    }
    
    func clearUserDefaults() {
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "success")
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "name")
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "company_code")
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "success")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControl.selectedSegmentIndex == 0 {
            if notInstalled.count == 0 {
                label.hidden = false
                label.text = "Geen nieuwe apps"
            } else {
                label.hidden = true
            }
            return notInstalled.count
        } else if segmentedControl.selectedSegmentIndex == 1 {
            if updates.count == 0 {
                label.hidden = false
                label.text = "Geen updates"
            }  else {
                label.hidden = true
            }
            return updates.count
        } else if segmentedControl.selectedSegmentIndex == 2 {
            if installed.count == 0 {
                label.hidden = false
                label.text = "Geen geïnstalleerde apps"
            }  else {
                label.hidden = true
            }
            return installed.count
        } else {
            return 1
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        navigationController?.navigationBar.showBottomHairline()
    }

    @IBAction func cellButtonPressed(sender: UIButton) {
        if sender.tag == 1 {
            installNewApp(sender)
        } else if sender.tag == 2 {
            openInstalledApp(sender)
        } else if sender.tag == 0 {
            installNewApp(sender)
        }
    }
    
    func openInstalledApp(sender: AnyObject) {
        if let button = sender as? UIButton {
            button.setTitleColor(UIColor.blackColor(), forState: .Normal)
            if let superview = button.superview {
                if let cell = superview.superview as? AppsTableViewCell {
                    if let appURL = NSURL(string: "\(cell.deepLink)://") {
                        let canOpen = UIApplication.sharedApplication().canOpenURL(appURL)
                        if canOpen {
                            UIApplication.sharedApplication().openURL(appURL)
                        }
                    }
                }
            }
        }
    }
    
    func installNewApp(sender: AnyObject) {
        var downloadUrl: String! = ""
        var app: AppsArray
        if let button = sender as? UIButton {
            if let superview = button.superview {
                if let cell = superview.superview as? AppsTableViewCell {
                    let position: CGPoint = sender.convertPoint(CGPointZero, toView: self.tableView)
                    if let _ = self.tableView.indexPathForRowAtPoint(position) {
                        downloadUrl = cell.downloadUrl!
                        if segmentedControl.selectedSegmentIndex == 1 {
                            app = updates[cell.tag]
                        } else {
                            app = notInstalled[cell.tag]
                        }
                        WeMaStoreAPI.sharedInstance.appInstallInfo(loginName, packageName: app.package_name, version: app.latestVersion!.version_no, action: "install") {
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
        let url = NSURL(string: "itms-services://?action=download-manifest&url=https://www.iappministrator.com/market/apks/\(downloadUrl!)")!
        let request = NSURLRequest(URL: url)
        webView.loadRequest(request)
        downloadingAlert()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! AppsTableViewCell
        cell.appImage.image = UIImage(named: "iOS_7_Icon_Template")
        cell.backgroundColor = cell.contentView.backgroundColor
        cell.nameLabel.text = setupTableViewItems(indexPath).name
        cell.versionLabel.text = "Versie: \(setupTableViewItems(indexPath).latestVersion!.version_name)"
        cell.buildLabel.text = "Build: \(setupTableViewItems(indexPath).latestVersion!.version_no)"
        cell.downloadUrl = setupTableViewItems(indexPath).latestVersion!.download_url
        if let appIcon = setupTableViewItems(indexPath).icon {
            let url = "http://www.iappministrator.com/market/icons/\(appIcon)"
            setupImage(url, superView: cell.appImage)
        }
        cell.deepLink = setupTableViewItems(indexPath).package_name
        cell.tag = indexPath.row
        setupButtons(cell)
        return cell
    }
    
    func setupTableViewItems(indexPath: NSIndexPath) -> AppsArray {
        var app = AppsArray()
        if segmentedControl.selectedSegmentIndex == 0 {
            app = notInstalled[indexPath.row]
        } else if segmentedControl.selectedSegmentIndex == 1 {
            app = updates[indexPath.row]
        } else if segmentedControl.selectedSegmentIndex == 2 {
            app = installed[indexPath.row]
        }
        return app
    }
    
    func setupButtons(cell: AppsTableViewCell) {
        if segmentedControl.selectedSegmentIndex == 1 {
            cell.deleteButton.setTitleWithOutAnimation("WERK BIJ")
            cell.buttonWidthConstant.constant = 85
            cell.deleteButton.tag = 1
        } else if segmentedControl.selectedSegmentIndex == 2 {
            cell.deleteButton.setTitleWithOutAnimation("OPEN")
            cell.buttonWidthConstant.constant = 50
            cell.deleteButton.tag = 2
        } else if segmentedControl.selectedSegmentIndex == 0 {
            cell.deleteButton.setTitleWithOutAnimation("DOWNLOAD")
            cell.buttonWidthConstant.constant = 95
            cell.deleteButton.tag = 0
        }
        cell.deleteButton.layer.borderColor = Color.sharedInstance.blueColor.CGColor
        cell.deleteButton.layer.borderWidth = 1
        cell.deleteButton.layer.cornerRadius = 4
        cell.deleteButton.clipsToBounds = true
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func setupImage(url: String, superView: UIImageView) {
        superView.hnk_setImageFromURL(NSURL(fileURLWithPath: url))
        let URL = NSURL(string: url)!
        let cache = Shared.imageCache
        let fetcher = NetworkFetcher<UIImage>(URL: URL)
        cache.fetch(fetcher: fetcher).onSuccess { image in
            superView.image = image
        }
        cache.fetch(fetcher: fetcher).onFailure({ (error) in
            superView.image = UIImage(named: "iOS_7_Icon_Template")
        })
    }
    
    func setLogOffButton(title: String) -> NSAttributedString {
        let attributedString: NSAttributedString
            attributedString = NSAttributedString(string: title, attributes: [
                NSFontAttributeName : UIFont.systemFontOfSize(16, weight: 0.4),NSForegroundColorAttributeName : Color.sharedInstance.blueColor])
        return attributedString
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        setupItems(indexPath)
        performSegueWithIdentifier("showApp", sender: self)
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func showUpdates() {
        performSegueWithIdentifier("showUpdates", sender: self)
    }
    
    func setupItems(indexPath: NSIndexPath) {
        if segmentedControl.selectedSegmentIndex == 0 {
            selectedApp = notInstalled[indexPath.row]
        } else if segmentedControl.selectedSegmentIndex == 1 {
            selectedApp = updates[indexPath.row]
        } else if segmentedControl.selectedSegmentIndex == 2 {
            selectedApp = installed[indexPath.row]
        }
        if segmentedControl.selectedSegmentIndex == 0 {
            buttonTitle = "DOWNLOAD"
        } else if segmentedControl.selectedSegmentIndex == 1 {
            buttonTitle = "WERK BIJ"
        } else if segmentedControl.selectedSegmentIndex == 2 {
            buttonTitle = "OPEN"
        }
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showApp" {
            let destinationVC = segue.destinationViewController as! InstallViewController
            destinationVC.buttonTitle = buttonTitle
            destinationVC.app = selectedApp
        } else if segue.identifier == "showSingleApp" {
            let navVC = segue.destinationViewController as! UINavigationController
            if let destinationVC = navVC.viewControllers[0] as? InstallViewController {
            destinationVC.app = selectedApp
            destinationVC.buttonTitle = buttonTitle
            destinationVC.type = "1"
            }
        }
    }

    func checkIfInstalled(dataItem: AppsArray) -> Bool {
        if let appURL = NSURL(string: "\(dataItem.package_name)://") {
            let canOpen = UIApplication.sharedApplication().canOpenURL(appURL)
            if canOpen {
                installed.append(dataItem)
                return true
            } else {
                return appendAppToNotInstalled(dataItem)
            }
        } else {
            notInstalled.append(dataItem)
            return false
        }
    }
    
    func checkForUpdate(dataItem: AppsArray) -> Bool {
        if let appURL = NSURL(string: "\(dataItem.package_name)://") {
            let canOpen = UIApplication.sharedApplication().canOpenURL(appURL)
            if canOpen {
                updates.append(dataItem)
                return true
            } else {
                return appendAppToNotInstalled(dataItem)
            }
        } else {
            notInstalled.append(dataItem)
            return false
        }
    }
    
    func appendAppToNotInstalled(item: AppsArray) -> Bool {
        notInstalled.append(item)
        for item in notInstalled {
            WeMaStoreAPI.sharedInstance.appInstallInfo(loginName, packageName: item.package_name, version: item.latestVersion!.version_no, action: "delete") {}
        }
        return false
    }
}

