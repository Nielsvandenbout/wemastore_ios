//
//  UpdateViewController.swift
//  WeMa Store
//
//  Created by Niels on 19-05-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import UIKit

class UpdateViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        showAlertView("Update", message: "Er is een nieuwe update voor de WeMa Store. Voordat u verdergaat moet u eerst de nieuwe versie op uw toestel installeren.")
    }
    
    func showAlertView(title: String, message: String) {
        let attributedString: NSAttributedString
        attributedString = NSAttributedString(string: title, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(16, weight: 0.4),NSForegroundColorAttributeName : Color.sharedInstance.blueColor])
        let attributedMessage = NSAttributedString(string: message, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(14),NSForegroundColorAttributeName : UIColor.lightGrayColor()])
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .Alert)
        alertController.setValue(attributedString, forKey: "attributedTitle")
        alertController.setValue(attributedMessage, forKey: "attributedMessage")
        alertController.addAction(UIAlertAction(title: "Update", style: .Default, handler: { (UIAlertAction) in
            let url = NSURL(string: "itms-services://?action=download-manifest&url=https://www.iappministrator.com/market/apks/\(NSUserDefaults.standardUserDefaults().stringForKey("download_url")!)")
            let request = NSURLRequest(URL: url!)
            self.webView.loadRequest(request)
        }))
        presentViewController(alertController, animated: true, completion: nil)
        showVisualEffect(alertController)
    }
    
    func showVisualEffect(superView: UIAlertController) {
        if let visualEffectView = superView.view.searchVisualEffectsSubview()
        {
            visualEffectView.effect = UIBlurEffect(style: .Dark)
        }
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }

}
