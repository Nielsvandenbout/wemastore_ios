import UIKit
import Haneke

protocol TableViewCellDelegate {
}

class TableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource, TableViewCellDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var installed = [AppsArray]()
    var notInstalled = [AppsArray]()
    var updates = [AppsArray]()
    var allApps = [AppsArray]()
    var topRatedVCDelegate: TopRatedViewControllerDelegate!
    var loginName: String!
    var buttonTitle: String!
    var refreshControl: UIRefreshControl!
    var apps: [AppsArray]?
    var app = AppsArray()
    var appSections = [AppSectionData]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if NSUserDefaults.standardUserDefaults().stringForKey("name") != nil {
            loginName = NSUserDefaults.standardUserDefaults().stringForKey("name")!
        }
        downloadData()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.contentInset = UIEdgeInsets(top: 8, left: 15, bottom: 8, right: 15)
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.tag == 0 {
            return allApps.count
        } else if self.tag == 1 {
            return updates.count
        } else {
            return installed.count
        }        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("Cell", forIndexPath: indexPath) as! CollectionViewCell
        cell.tableViewCellDelegate = self
        cell.image.image = UIImage()
        return setupCell(cell, indexPath: indexPath)
    }
    
    func setupCell(collectionCell: CollectionViewCell, indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionCell
        cell.tag = indexPath.row
        let noAppsLabel = UILabel(frame: self.frame)
        noAppsLabel.text = "Geen apps"
        noAppsLabel.textColor = UIColor.whiteColor()
        noAppsLabel.font = UIFont.systemFontOfSize(25)
        if self.tag == 0 {
            if allApps.count > 0 {
                cell.apps = allApps
                cell.text.text = cell.apps[indexPath.item].name
                cell.versionLabel.text = cell.apps[indexPath.item].latestVersion!.version_name
                let appIcon = cell.apps[indexPath.item].icon
                if cell.tag == indexPath.row {
                    setupImage(cell.image, appIcon: appIcon)
                } else {
                    cell.image.image = UIImage(named: "iOS_7_Icon_Template")
                }
                cell.tag = indexPath.row
                return cell
            } else {
                self.addSubview(noAppsLabel)
                self.bringSubviewToFront(noAppsLabel)
            }
        } else if self.tag == 1 {
            if updates.count > 0 {
                cell.apps = updates
                cell.text.text = cell.apps[indexPath.item].name
                cell.versionLabel.text = cell.apps[indexPath.item].latestVersion!.version_name
                let appIcon = cell.apps[indexPath.item].icon
                if cell.tag == indexPath.row {
                    setupImage(cell.image, appIcon: appIcon)
                } else {
                    cell.image.image = UIImage(named: "iOS_7_Icon_Template")
                }
                cell.tag = indexPath.row
                return cell
            } else {
                self.addSubview(noAppsLabel)
                self.bringSubviewToFront(noAppsLabel)
            }
        } else if self.tag == 2 {
            if installed.count > 0 {
                cell.apps = installed
                cell.text.text = cell.apps[indexPath.item].name
                cell.versionLabel.text = cell.apps[indexPath.item].latestVersion!.version_name
                let appIcon = cell.apps[indexPath.item].icon
                if cell.tag == indexPath.row {
                    setupImage(cell.image, appIcon: appIcon)
                } else {
                    cell.image.image = UIImage(named: "iOS_7_Icon_Template")
                }
                cell.tag = indexPath.row
                return cell
            } else {
                self.addSubview(noAppsLabel)
                self.bringSubviewToFront(noAppsLabel)
            }
        } else {
            return cell
        }
        return cell
    }
    
    func setupImage(superView: UIImageView, appIcon: String?) {
        superView.image = UIImage(named: "iOS_7_Icon_Template")
        let url = "http://www.iappministrator.com/market/icons/\(appIcon!)"
        if appIcon != "" {
            superView.hnk_setImageFromURL(NSURL(fileURLWithPath: url))
            let URL = NSURL(string: url)!
            let cache = Shared.imageCache
            let fetcher = NetworkFetcher<UIImage>(URL: URL)
            cache.fetch(fetcher: fetcher).onSuccess { image in
                superView.image = image
            }
            cache.fetch(fetcher: fetcher).onFailure({ (error) in
                superView.image = UIImage(named: "iOS_7_Icon_Template")
            })
        } else {
            superView.image = UIImage(named: "iOS_7_Icon_Template")
        }
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if self.tag == 0 {
            app = allApps[indexPath.row]
        } else if self.tag == 1 {
            app = updates[indexPath.row]
        } else if self.tag == 2 {
            app = installed[indexPath.row]
        }
        if self.tag == 1 {
            buttonTitle = "WERK BIJ"
        } else if self.tag == 2 {
            buttonTitle = "OPEN"
        } else if self.tag == 0 {
            buttonTitle = "DOWNLOAD"
        }
        topRatedVCDelegate.showApp(app)
    }

    func downloadData() {
        if WeMaStoreAPI.sharedInstance.installed.count < 1 && WeMaStoreAPI.sharedInstance.notInstalled.count < 1 && WeMaStoreAPI.sharedInstance.updates.count < 1 {
        let company_code = NSUserDefaults.standardUserDefaults().stringForKey("company_code")!
        if Reachability.isConnectedToNetwork() {
            WeMaStoreAPI.sharedInstance.getApps(loginName, owner_code: company_code) { (responseObject, error) in
                if let response = responseObject?.newApps {
                    self.clearArrays()
                    for newApp in response {
                        self.checkIfInstalled(newApp)
                    }
                }
                if let response = responseObject?.installedApps {
                    for newApp in response {
                        self.checkIfInstalled(newApp)
                    }
                }
                if let response = responseObject?.updateableApps {
                    for newApp in response {
                        self.checkForUpdate(newApp)
                    }
                }
                self.appendAllApps()
                self.setData()
                self.collectionView.reloadData()
            }
        }
        } else {
            self.clearArrays()
            installed = WeMaStoreAPI.sharedInstance.installed
            notInstalled = WeMaStoreAPI.sharedInstance.notInstalled
            updates = WeMaStoreAPI.sharedInstance.updates
            self.appendAllApps()
            self.setData()
            self.collectionView.reloadData()
        }
    }
    
    func appendAllApps() {
        for app in self.installed {
            self.allApps.append(app)
        }
        for app in self.notInstalled {
            self.allApps.append(app)
        }
        for app in self.updates {
            self.allApps.append(app)
        }
    }
    
    func setData() {
        let installedSection = AppSectionData(title: "Geïnstalleerd", data: installed)
        let updatesSection = AppSectionData(title: "Beschikbare Updates", data: updates)
        let notInstalledSection = AppSectionData(title: "Nieuwe Apps", data: notInstalled)
        appSections.append(updatesSection)
        appSections.append(notInstalledSection)
        appSections.append(installedSection)
    }
    
    func clearArrays() {
        installed.removeAll()
        notInstalled.removeAll()
        updates.removeAll()
        allApps.removeAll()
        appSections.removeAll()
    }
    
    func checkIfInstalled(dataItem: AppsArray) -> Bool {
        if let appURL = NSURL(string: "\(dataItem.package_name)://") {
            let canOpen = UIApplication.sharedApplication().canOpenURL(appURL)
            if canOpen {
                installed.append(dataItem)
                return true
            } else {
                notInstalled.append(dataItem)
                return false
            }
        } else {
            notInstalled.append(dataItem)
            return false
        }
    }
    
    func checkForUpdate(dataItem: AppsArray) -> Bool {
        if let appURL = NSURL(string: "\(dataItem.package_name)://") {
            let canOpen = UIApplication.sharedApplication().canOpenURL(appURL)
            if canOpen {
                updates.append(dataItem)
                return true
            } else {
                notInstalled.append(dataItem)
                return false
            }
        } else {
            notInstalled.append(dataItem)
            return false
        }
    }
    
    func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showApp" {
            let destinationVC = segue.destinationViewController as! InstallViewController
            destinationVC.app = app
            destinationVC.buttonTitle = buttonTitle
        }
    }
}

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var text: UILabel!
    var apps = [AppsArray]()
    var tableViewCellDelegate: TableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        image.layer.cornerRadius = 18
        image.clipsToBounds = true
        image.backgroundColor = UIColor.lightGrayColor()
    }
    
}
