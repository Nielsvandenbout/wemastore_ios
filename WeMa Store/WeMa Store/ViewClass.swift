//
//  ViewClass.swift
//  WeMaMobile
//
//  Created by Niels van den Bout on 07-04-16.
//  Copyright © 2016 Niels van den Bout. All rights reserved.
//

import UIKit

class ViewClass: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 30
    }

}
