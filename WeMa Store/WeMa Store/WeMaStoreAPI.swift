//
//  WeMaStoreAPI.swift
//  WeMa Store
//
//  Created by Niels on 13-05-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import Foundation
import Alamofire

let BASE_URL = "http://www.iappministrator.com/market/api/"
let LOGIN = "check_login.php"
let GET_APPS = "fetch_app_list.php"
let CHECK_STORE_VERSION = "self_update_check.php"
let ADJUST_INSTALL_INFO = "adjust_app_install_info.php"
let UPDATE_PUSH_ID = "update_push_id.php"


class WeMaStoreAPI {
    class var sharedInstance: WeMaStoreAPI {
        struct Singleton {
            static let instance = WeMaStoreAPI()
        }
        return Singleton.instance
    }
    
    var installed = [AppsArray]()
    var notInstalled = [AppsArray]()
    var updates = [AppsArray]()
    
    let loginUrl = "\(BASE_URL)\(LOGIN)"
    let appsUrl = "\(BASE_URL)\(GET_APPS)"
    let checkUrl = "\(BASE_URL)\(CHECK_STORE_VERSION)"
    let adjustInfoUrl = "\(BASE_URL)\(ADJUST_INSTALL_INFO)"
    let updatePushUrl = "\(BASE_URL)\(UPDATE_PUSH_ID)"
    
    func Login(user: AnyObject, companyCode: AnyObject, completionHandler: (responseObject: LoginResponse?, error: NSError?) -> ()) {
        Alamofire.request(.POST, loginUrl, parameters: ["user_name" : user, "company_code" : companyCode]).responseObject { (response: Response<LoginResponse, NSError>) in
            completionHandler(responseObject: response.result.value, error: response.result.error)
        }
    }
    
    func checkStoreVersion(completionHandler: (responseObject: CheckStoreResponse?, error: NSError?) -> ()) {
        Alamofire.request(.POST, checkUrl, parameters: ["platform": "iOS"]).responseObject { (response: Response<CheckStoreResponse, NSError>) in
            completionHandler(responseObject: response.result.value, error: response.result.error)
        }
    }
    
    func getApps(user_name: AnyObject, owner_code: AnyObject, completionHandler: (responseObject: Apps?, error: NSError?) -> ()) {
        Alamofire.request(.POST, appsUrl, parameters: ["user_name": user_name, "owner_code": owner_code, "platform": "iOS"]).responseObject { (response: Response<Apps, NSError>) in
            completionHandler(responseObject: response.result.value, error: response.result.error)
        }
    }
    
    func appInstallInfo(user_name: AnyObject, packageName: AnyObject, version: AnyObject, action: AnyObject, completionHandler: () -> ()) {
        Alamofire.request(.POST, adjustInfoUrl, parameters: ["user_name": user_name, "packageName": packageName, "version": version, "action": action])
    }
    
    func updatePushID(user_name: AnyObject, push_id: AnyObject) {
        Alamofire.request(.POST, updatePushUrl, parameters: ["user_name": user_name, "push_id": push_id])
    }
}
