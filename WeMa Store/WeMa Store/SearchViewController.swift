//
//  SearchViewController.swift
//  WeMa Store
//
//  Created by Niels van den Bout on 28-06-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet var appButtons: [UIButton]!
    
    var loginName: String!
    var apps = [AppsArray]()
    var count = 0
    var app: AppsArray!
    lazy var searchBar = UISearchBar(frame: CGRectZero)

    override func viewDidLoad() {
        super.viewDidLoad()
        if NSUserDefaults.standardUserDefaults().stringForKey("name") != nil {
            loginName = NSUserDefaults.standardUserDefaults().stringForKey("name")!
        }
        navigationItem.backBarButtonItem?.title = "Terug"
        self.navigationController?.clearNavBar()
        let company_code = NSUserDefaults.standardUserDefaults().stringForKey("company_code")!
        if Reachability.isConnectedToNetwork() {
            WeMaStoreAPI.sharedInstance.getApps(loginName, owner_code: company_code) { (responseObject, error) in
                if responseObject != nil {
                    for app in (responseObject?.newApps)! {
                        self.apps.append(app)
                    }
                    for app in (responseObject?.updateableApps)! {
                        self.apps.append(app)
                    }
                    if self.apps.count > 5 {
                        for button in self.appButtons[0..<5] {
                            button.setTitleWithOutAnimation(self.apps[self.count].name)
                            self.count += 1
                        }
                    } else {
                        for button in self.appButtons[0..<self.apps.count] {
                            button.setTitleWithOutAnimation(self.apps[self.count].name)
                            self.count += 1
                        }
                    }
                    for button in self.appButtons {
                        if button.titleLabel?.text == nil {
                            button.hidden = true
                            button.enabled = false
                        }
                    }
                }
            }
        } else {
            showAlertView("Geen internet", message: "U heeft op dit moment geen internetverbinding.")
        }
        searchBar.placeholder = "De zoekfunctie is tijdelijk uitgeschakeld."
        searchBar.barStyle = UIBarStyle.Black
        searchBar.keyboardAppearance = .Dark
        navigationItem.titleView = searchBar
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func showAlertView(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Oke", style: UIAlertActionStyle.Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func dismissKeyboard() {
        searchBar.endEditing(true)
    }
    
    @IBAction func buttonPressed(sender: UIButton) {
        for button in appButtons {
            if button.hidden == false {
                if button.titleLabel?.text == apps[sender.tag].name && sender.tag < apps.count {
                    app = apps[button.tag]
                    if searchBar.isFirstResponder() {
                        searchBar.endEditing(true)
                    }
                    performSegueWithIdentifier("showFoundApp", sender: self)
                }   
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showFoundApp" {
            let buttonTitle = "OPEN"
            let destinationVC = segue.destinationViewController as! InstallViewController
            destinationVC.app = app
            destinationVC.buttonTitle = buttonTitle
        }
    }
}
