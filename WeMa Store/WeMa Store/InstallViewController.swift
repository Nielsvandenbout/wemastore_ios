//
//  InstallViewController.swift
//  WeMa Store
//
//  Created by Niels on 17-05-16.
//  Copyright © 2016 Niels. All rights reserved.
//



import UIKit
import Haneke

class InstallViewController: UIViewController {
    
    @IBOutlet weak var buttonConstraint: NSLayoutConstraint!
    @IBOutlet weak var appImage: UIImageView!
    @IBOutlet weak var InstallButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var buildLabel: UILabel!
    @IBOutlet weak var appDescriptionTextView: UITextView!
    
    var app = AppsArray()
    var buttonTitle = ""
    let loginName = NSUserDefaults.standardUserDefaults().stringForKey("name")!
    var type: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews() {
        nameLabel.text = app.name
        webView.layer.cornerRadius = 22
        webView.clipsToBounds = true
        buildLabel.text = "Build: \(app.latestVersion!.version_no)"
        versionLabel.text = "Versie: \(app.latestVersion!.version_name)"
        showSingleAppDesign()
        setupTextView()
        setupAppImage()
        setupButtons()
    }
    
    func setupButtons() {
        InstallButton.setTitleWithOutAnimation(buttonTitle)
        InstallButton.layer.borderWidth = 1
        InstallButton.layer.borderColor = Color.sharedInstance.blueColor.CGColor
        InstallButton.layer.cornerRadius = 4
        
        if buttonTitle == "WERK BIJ" {
            buttonConstraint.constant = 85
        } else if buttonTitle == "DOWNLOAD" {
            buttonConstraint.constant = 95
        } else {
            buttonConstraint.constant = 50
        }
    }
    
    func setupAppImage() {
        appImage.layer.cornerRadius = 22
        appImage.layer.borderColor = UIColor.lightGrayColor().CGColor
        appImage.layer.borderWidth = 0.0
        appImage.clipsToBounds = true
        downloadImage()
    }
    
    func setupTextView() {
        appDescriptionTextView.text = app.latestVersion!.release_text
        appDescriptionTextView.editable = true
        appDescriptionTextView.textColor = UIColor.lightGrayColor()
        appDescriptionTextView.font = UIFont.systemFontOfSize(14)
        appDescriptionTextView.editable = false
    }
    
    func downloadImage() {
        let url = "http://www.iappministrator.com/market/icons/\(app.icon)"
        appImage.hnk_setImageFromURL(NSURL(fileURLWithPath: url))
        if let URL = NSURL(string: url) {
            let cache = Shared.imageCache
            let fetcher = NetworkFetcher<UIImage>(URL: URL)
            cache.fetch(fetcher: fetcher).onSuccess { image in
                self.appImage.image = image
            }
        }
    }
    
    func showSingleAppDesign() {
        if type != nil {
            let negativeSpacer = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FixedSpace, target: nil, action: nil)
            negativeSpacer.width = 5
            let barButtonItem = UIBarButtonItem(title: "Uitloggen", style: .Plain, target: self, action: #selector(self.logoff))
            self.navigationItem.setLeftBarButtonItems([negativeSpacer, barButtonItem], animated: false)
            self.navigationController?.clearNavBar()
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Action, target: self, action: #selector(showOptAlert))
    }
    
    @IBAction func installApp(sender: AnyObject) {
        tryToInstallApp()
    }
    
    func tryToInstallApp() {
        if buttonTitle == "DOWNLOAD" || buttonTitle == "WERK BIJ" {
            let url = NSURL(string: "itms-services://?action=download-manifest&url=https://www.iappministrator.com/market/apks/\(app.latestVersion!.download_url)")
            let request = NSURLRequest(URL: url!)
            webView.loadRequest(request)
            WeMaStoreAPI.sharedInstance.appInstallInfo(loginName, packageName: app.package_name, version: (app.latestVersion?.version_no)!, action: "install") {}
            downloadingAlert()
        } else {
            if let appURL = NSURL(string: "\(app.package_name)://") {
                let canOpen = UIApplication.sharedApplication().canOpenURL(appURL)
                if canOpen {
                    UIApplication.sharedApplication().openURL(appURL)
                }
            } else {
                showAlertView("OEPS", message: "Deze app is niet meer geïnstalleerd, of er is iets fout gegaan. Probeer het later opnieuw.")
            }
        }
    }
    
    func showOptAlert() {
        let downloadUrl = NSURL(string: "itms-services://?action=download-manifest&url=https://www.iappministrator.com/market/apks/\(app.latestVersion!.download_url)")!
        print(downloadUrl)
        let optionAlert = UIAlertController(title: nil, message: nil, preferredStyle: .ActionSheet)
        optionAlert.addAction(UIAlertAction(title: "Download opnieuw", style: .Default, handler: { (UIAlertAction) in
            UIApplication.sharedApplication().openURL(downloadUrl)
            
            WeMaStoreAPI.sharedInstance.appInstallInfo(self.loginName, packageName: self.app.package_name, version: (self.app.latestVersion!.version_no)!, action: "installed") {}
            self.downloadingAlert()
        }))
        optionAlert.addAction(UIAlertAction(title: "Annuleer", style: .Default, handler: nil))
        optionAlert.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        presentViewController(optionAlert, animated: true) {
        }
        showVisualEffect(optionAlert)
    }
    
    func showVisualEffect(superView: UIAlertController) {
        if let visualEffectView = superView.view.searchVisualEffectsSubview()
        {
            visualEffectView.effect = UIBlurEffect(style: .Dark)
        }
    }
    
    func showAlertView(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "oke", style: UIAlertActionStyle.Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func logoff() {
        let attributedMessage = NSAttributedString(string: "Weet u zeker dat u wilt uitloggen?", attributes: [
            NSFontAttributeName : UIFont.systemFontOfSize(14),
            NSForegroundColorAttributeName : UIColor.lightGrayColor()
            ])
        let alertController = UIAlertController(title: "", message: "", preferredStyle: .Alert)
        alertController.setValue(setLogOffButton("Uitloggen"), forKey: "attributedTitle")
        alertController.setValue(attributedMessage, forKey: "attributedMessage")
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (UIAlertAction) in
            self.clearUserDefaults()
            if #available(iOS 9, *) {
                let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                let navBar = self.storyboard?.instantiateViewControllerWithIdentifier("loginVC")
                self.navigationController?.popToRootViewControllerAnimated(true)
                appDelegate.window?.rootViewController = navBar
            }
            NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "success")
        }))
        alertController.addAction(UIAlertAction(title: "Annuleren", style: .Cancel, handler: nil))
        presentViewController(alertController, animated: true) {
        }
        showVisualEffect(alertController)
    }
    
    func clearUserDefaults() {
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "success")
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "name")
        NSUserDefaults.standardUserDefaults().setObject(nil, forKey: "company_code")
    }
    
    func downloadingAlert() {
        let attributedMessage = NSAttributedString(string: "Wanneer u op downloaden heeft gedrukt, gaat u naar het homescreen om de installatie te bekijken. Als u op annuleren heeft gedrukt, kunt u dit bericht negeren.", attributes: [
            NSFontAttributeName : UIFont.systemFontOfSize(14),
            NSForegroundColorAttributeName : UIColor.lightGrayColor()
            ])
        let alertController = UIAlertController(title: "Downloaden", message: "", preferredStyle: .Alert)
        alertController.setValue(setLogOffButton("Downloaden"), forKey: "attributedTitle")
        alertController.setValue(attributedMessage, forKey: "attributedMessage")
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: nil))
        presentViewController(alertController, animated: true) {
        }
        showVisualEffect(alertController)
    }
    
    func setLogOffButton(title: String) -> NSAttributedString {
        let attributedString: NSAttributedString
            attributedString = NSAttributedString(string: title, attributes: [NSFontAttributeName : UIFont.systemFontOfSize(16, weight: 0.4),NSForegroundColorAttributeName : Color.sharedInstance.blueColor])
        return attributedString
    }
}