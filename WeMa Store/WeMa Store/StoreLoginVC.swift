//
//  LoginViewController.swift
//  WeMa Store
//
//  Created by Niels on 13-05-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import AVKit
import AVFoundation
import IQKeyboardManagerSwift

class StoreLoginVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var blur: UIVisualEffectView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var companyCodeTextField: UITextField!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loginButton: UIButton!
    
    var keyboardisHidden = true
    var player: AVPlayer?
    var returnKeyHandler: IQKeyboardReturnKeyHandler!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    func setupViews() {
        blur.layer.cornerRadius = 30
        blur.clipsToBounds = true
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(StoreLoginVC.dismissKeyboard))
        view.addGestureRecognizer(tap)
        returnKeyHandler = IQKeyboardReturnKeyHandler(controller: self)
        returnKeyHandler.lastTextFieldReturnKeyType = UIReturnKeyType.Done
        IQKeyboardManager.sharedManager().keyboardAppearance = .Dark
        IQLayoutGuideConstraint?.constant = 10
        IQBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : UIColor.whiteColor()], forState: .Normal)
        nameTextField.attributedPlaceholder = NSAttributedString(string:"Gebruikersnaam", attributes:[NSForegroundColorAttributeName: UIColor.init(white: 256/256, alpha: 0.5)])
        companyCodeTextField.attributedPlaceholder = NSAttributedString(string:"Bedrijfscode", attributes:[NSForegroundColorAttributeName: UIColor.init(white: 256/256, alpha: 0.5)])
        companyCodeTextField.delegate = self
        nameTextField.delegate = self
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    
    func textFieldDidEndEditing(textField: UITextField) {
        if nameTextField.text?.characters.count > 0 && companyCodeTextField.text?.characters.count > 0 {
            loginButton.alpha = 1.0
        } else {
            loginButton.alpha = 0.5
        }
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if nameTextField.text?.characters.count > 0 && companyCodeTextField.isFirstResponder() {
            loginButton.alpha = 1.0
        }
        if companyCodeTextField.text?.characters.count > 0 && nameTextField.isFirstResponder() {
            loginButton.alpha = 1.0
        }
    }

    @IBAction func loginButtonTouched(sender: AnyObject) {
        UIView.setAnimationsEnabled(true)
     
        self.loadingView.hidden = false
        self.activityIndicator.startAnimating()
        if Reachability.isConnectedToNetwork() == true {
            
            WeMaStoreAPI.sharedInstance.Login(nameTextField.text!, companyCode: companyCodeTextField.text!) { (responseObject, error) in
                if responseObject?.success == true {
                    PushNotificationManager.pushManager().registerForPushNotifications()
                    let success = responseObject?.success

                    NSUserDefaults.standardUserDefaults().setObject(success, forKey: "success")
                    NSUserDefaults.standardUserDefaults().setObject(self.nameTextField.text!, forKey: "name")
                    NSUserDefaults.standardUserDefaults().setObject(self.companyCodeTextField.text!, forKey: "company_code")
                    
                    self.loadingView.hidden = true
                    self.activityIndicator.stopAnimating()
                    
                    if #available(iOS 9, *) {
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        let tabBar = self.storyboard?.instantiateViewControllerWithIdentifier("tabVC") as! UITabBarController
                        appDelegate.window?.rootViewController = tabBar
                    }
                    
                } else if responseObject?.success == false {
                    self.loadingView.hidden = true
                    self.activityIndicator.stopAnimating()
                    self.showAlertView("oeps", message: "U heeft een verkeerde naam of bedrijfscode ingevuld.")

                } else if responseObject?.success == nil {
                    self.loadingView.hidden = true
                    self.activityIndicator.stopAnimating()
                    self.showAlertView("oeps", message: "U heeft een verkeerde naam of bedrijfscode ingevuld.")

                }
            }
        } else {
            showAlertView("oeps", message: "U heeft geen internetverbinding, probeer het later opnieuw.")
        }

    }
    
    func showAlertView(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Oke", style: UIAlertActionStyle.Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
}
