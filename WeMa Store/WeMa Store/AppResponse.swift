//
//  AppResponse.swift
//  WeMa Store
//
//  Created by Niels on 20-05-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import Foundation

final class LoginResponse: ResponseObjectSerializable {
    
    var success: Bool!
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let successObject = representation.valueForKeyPath("success") as? Bool {
            self.success = successObject
        } else {
            self.success = nil
        }
    }
}


final class Apps: ResponseObjectSerializable {
   
    var installedApps: [AppsArray]?
    var updateableApps: [AppsArray]?
    var newApps:[AppsArray]?

    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let installedAppsObject: AnyObject = representation.valueForKeyPath("installed") {
            self.installedApps = AppsArray.collection(response: response, representation: installedAppsObject)
        } else {
            self.installedApps = nil
        }
        if let updateableAppsObject: AnyObject = representation.valueForKeyPath("updates") {
            self.updateableApps = AppsArray.collection(response: response, representation: updateableAppsObject)
        } else {
            self.updateableApps = nil
        }
        if let newAppsObject: AnyObject = representation.valueForKeyPath("newApps") {
            self.newApps = AppsArray.collection(response: response, representation: newAppsObject)
        } else {
            self.newApps = nil
        }
    }
    
}


final class AppsArray: ResponseObjectSerializable, ResponseCollectionSerializable {
    var name: String!
    var package_name: String!
    var owner: String!
    var platform: String!
    var icon: String!
    var latestVersion: LatestVersion?
    
    init(name: String, package_name: String, owner: String, platform: String, icon: String, latestVersion: LatestVersion) {
        self.name = name
        self.package_name = package_name
        self.owner = owner
        self.platform = platform
        self.icon = icon
        self.latestVersion = latestVersion
    }
    
    init() {
        
    }
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let nameObject = representation.valueForKeyPath("name") as? String {
            self.name = nameObject
        } else {
            self.name = ""
        }
        if let package_nameObject = representation.valueForKeyPath("package_name") as? String {
            self.package_name = package_nameObject
        } else {
            self.package_name = ""
        }
        if let ownerObject = representation.valueForKeyPath("owner") as? String {
            self.owner = ownerObject
        } else {
            self.owner = ""
        }
        if let platformObject = representation.valueForKeyPath("platform") as? String {
            self.platform = platformObject
        } else {
            self.platform = ""
        }
        if let iconObject = representation.valueForKeyPath("icon") as? String {
            self.icon = iconObject
        } else {
            self.icon = ""
        }
        if let latestVersionObject: AnyObject = representation.valueForKeyPath("latestVersion") {
            self.latestVersion = LatestVersion(response: response, representation: latestVersionObject)
        } else {
            self.latestVersion = nil
        }

    }
    
    internal static func collection(response response: NSHTTPURLResponse, representation: AnyObject) -> [AppsArray] {
        var objects: [AppsArray] = []
        if let representation = representation as? [[String: AnyObject]] {
            for userRepresentation in representation {
                if let item = AppsArray(response: response, representation: userRepresentation) {
                    objects.append(item)
                }
            }
        }
        return objects
    }
    
}

final class LatestVersion: ResponseObjectSerializable {
    var version_no: String!
    var version_name: String!
    var version_type: String!
    var release_date: String!
    var release_text: String!
    var download_url: String!
    
    init(version_no: String, version_name: String, release_date: String, release_text: String, download_url: String) {
        self.version_no = version_no
        self.version_name = version_name
        self.release_date = release_date
        self.release_text = release_text
        self.download_url = download_url
    }
    
    init() {
        
    }
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let version_noObject = representation.valueForKeyPath("version_no") as? String {
            self.version_no = version_noObject
        } else {
            self.version_no = ""
        }
        if let version_nameObject = representation.valueForKeyPath("version_name") as? String {
            self.version_name = version_nameObject
        } else {
            self.version_name = ""
        }
        if let version_typeObject = representation.valueForKeyPath("version_type") as? String {
            self.version_type = version_typeObject
        } else {
            self.version_type = ""
        }
        if let release_dateObject = representation.valueForKeyPath("release_date") as? String {
            self.release_date = release_dateObject
        } else {
            self.release_date = nil
        }
        if let release_textObject = representation.valueForKeyPath("release_text") as? String {
            self.release_text = release_textObject
        } else {
            self.release_text = ""
        }
        if let download_urlObject = representation.valueForKeyPath("download_url") as? String {
            self.download_url = download_urlObject
        } else {
            self.download_url = ""
        }
    }
}

final class CheckStoreResponse: ResponseObjectSerializable {
    var icon: String!
    var latestVersion: StorelatestVersion?
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let iconObject = representation.valueForKeyPath("icon") as? String {
            self.icon = iconObject
        } else {
            self.icon = ""
        }
        if let latestVersionObject: AnyObject = representation.valueForKeyPath("latestVersion") {
            self.latestVersion = StorelatestVersion(response: response, representation: latestVersionObject)
        } else {
            self.latestVersion = nil
        }
    }
}

final class StorelatestVersion: ResponseObjectSerializable {
    var version_no: String!
    var download_url: String!
    var release_text: String!
    
    init?(response: NSHTTPURLResponse, representation: AnyObject) {
        if let version_noObject = representation.valueForKeyPath("version_no") as? String {
            self.version_no = version_noObject
        } else {
            self.version_no = ""
        }
        if let download_urlObject = representation.valueForKeyPath("download_url") as? String {
            self.download_url = download_urlObject
        } else {
            self.download_url = ""
        }
        if let release_textObject = representation.valueForKeyPath("release_text") as? String {
            self.release_text = release_textObject
        } else {
            self.release_text = ""
        }
    }
}

