//
//  WMImageSlider.swift
//  HeraclesAlmelo
//
//  Created by Martijn Smit on 24/03/16.
//  Copyright © 2016 WeMa Mobile B.V. All rights reserved.
//

import UIKit
import Haneke

enum PageControlLocation {
    case Left, Right, Center
}

class WMImageSlider: UIView {
    
    var images: [String] = []
    var scrollDirectionLandscape: Bool = false
    var showPageControl: Bool = true
    var isAutoScroll: Bool = false
    var pageControlLocation: PageControlLocation = .Center
    var imageViewCount: Int = 3
    var timer: NSTimer? = nil
    var imageSelectedCallBack: ((index: Int) -> ())?
    var selectedImage = ""
    
    // MARK: Lazy scrollview and pagecontrol
    lazy var scrollView: UIScrollView? = {
        let scrollView = UIScrollView()
        scrollView.pagingEnabled = true
        scrollView.delegate = self
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false

        for i in 0..<self.imageViewCount {
            let imageView = UIImageView()
            imageView.contentMode = UIViewContentMode.ScaleAspectFit
            imageView.userInteractionEnabled = true
            scrollView.addSubview(imageView)
        }
        return scrollView
    }()
    
    lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.currentPage = 0
        pageControl.hidden = true
        return pageControl
    }()
    
    /**
    Load images with callback
    - parameters
    - images: String
    */
    
    convenience init(images: [String], imageSelectedCallback: (index: Int) -> ()){
        self.init()
        
        self.imageSelectedCallBack = imageSelectedCallback
        
        // set images
        for i in 0..<imageViewCount { //*
            let imageView = scrollView?.subviews[i] as? UIImageView
            imageView?.image = UIImage(named: images[i])
            selectedImage = images[i]
        }
        
        self.images = images
        pageControl.numberOfPages = images.count
        self.addSubview(scrollView!)
        
        self.addSubview(pageControl)
        addChildViewConstraints()

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        scrollView?.frame = self.bounds
        
        if scrollDirectionLandscape == true {
            scrollView?.contentSize = CGSize(width: self.bounds.size.width * CGFloat(imageViewCount), height: 0) // [<->]
        } else {
            scrollView?.contentSize = CGSize(width: 0, height: self.bounds.size.height * CGFloat(imageViewCount)) // up
        }
        
        for i in 0..<imageViewCount {
            let imageView = scrollView!.subviews[i]
            if scrollDirectionLandscape == true {
                imageView.frame = CGRect(x: CGFloat(i) * scrollView!.frame.size.width, y: 0, width: scrollView!.frame.size.width, height: scrollView!.frame.size.height)
            } else {
                imageView.frame = CGRect(x: 0, y: CGFloat(i) * scrollView!.frame.size.height, width: scrollView!.frame.size.width, height: scrollView!.frame.size.height)
            }
        }
        
        updateContent()
    }
    
    // init imageSlider
    class func imageSlider(images: [String]) -> WMImageSlider {
        let imageSlide = WMImageSlider(images: images) { (index) -> () in
        }
        return imageSlide
    }
    
    // Constraints
    private func addChildViewConstraints() {
        let dict = ["self": self, "pageControl": pageControl]
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        
        var position: String?
        switch pageControlLocation {
        case .Left:
            position = "H:|-[pageControl]"
        case .Right:
            position = "H:[pageControl-|"
        case .Center:
            position = "H:|[pageControl]|" //H:|-[pageControl]-|
        }
        
        let layoutHorizontal = NSLayoutConstraint.constraintsWithVisualFormat(position!, options: .DirectionLeadingToTrailing, metrics: nil, views: dict)
        self.addConstraints(layoutHorizontal)
        
        let layoutVertical = NSLayoutConstraint.constraintsWithVisualFormat("V:[pageControl(15)]-(0)-|", options: .DirectionLeadingToTrailing, metrics: nil, views: dict) // V:[pageControl(15)]-(20)-|
        self.addConstraints(layoutVertical)
    }
    
    // Content
    func updateContent() {
        for i in 0..<imageViewCount {
            let imageView = scrollView?.subviews[i] as? UIImageView
            var indexPage = pageControl.currentPage
            if i == 0 {
                indexPage -= 1
            } else if i == 2 {
                indexPage += 1
            }
            if indexPage < 0 {
                indexPage = pageControl.numberOfPages - 1
            } else if indexPage >= pageControl.numberOfPages {
                indexPage = 0
            }

            imageView?.image = UIImage(named: self.images[indexPage])
            print(imageView?.image?.size.width)
            imageView?.contentMode = .ScaleAspectFill
            imageView?.frame.size.width = UIScreen.mainScreen().bounds.width
            imageView?.tag = indexPage
            imageView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showImageInfo(_:))))

        }
        scrollView!.contentOffset = scrollDirectionLandscape ? CGPointMake(scrollView!.frame.size.width, 0): CGPointMake(0, scrollView!.frame.size.height)
    }
    
    private func startTimer() {
        let timer = NSTimer(timeInterval: 2, target: self, selector: Selector(nextPage()), userInfo: nil, repeats: true)
        self.timer = timer
        NSRunLoop.mainRunLoop().addTimer(timer, forMode: NSRunLoopCommonModes)
    }
    
    private func stopTimer() {
        timer?.invalidate()
        timer = nil
    }
    
    private func nextPage() {
        if scrollDirectionLandscape == false {
            scrollView?.setContentOffset(CGPoint(x: 0, y: 2 * scrollView!.frame.size.height), animated: true)
        } else {
            scrollView!.setContentOffset(CGPoint(x: 2 * scrollView!.frame.size.width, y: 0), animated: true)
        }
    }
    
    func showImageInfo(tap: UITapGestureRecognizer) {
        imageSelectedCallBack!(index: pageControl.currentPage)
        print(images[pageControl.currentPage])
    }
}

extension WMImageSlider: UIScrollViewDelegate {
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        updateContent()
    }
    
    func scrollViewDidEndScrollingAnimation(scrollView: UIScrollView) {
        updateContent()
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        var page = 0
        var minDistance = CGFloat(MAXFLOAT)
        for i in 0..<3 {
            let imageView = scrollView.subviews[i] as? UIImageView
            let distance = scrollDirectionLandscape ? abs(imageView!.frame.origin.x - scrollView.contentOffset.x): abs(imageView!.frame.origin.y - scrollView.contentOffset.y)
            if distance < minDistance {
                minDistance = distance
                page = imageView!.tag
            }
        }
        pageControl.currentPage = page
    }
    
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if isAutoScroll {
            startTimer()
        }
    }
    
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        if isAutoScroll {
            stopTimer()
        }
    }
}