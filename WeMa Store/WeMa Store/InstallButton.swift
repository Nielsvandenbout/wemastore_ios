//
//  InstallButton.swift
//  WeMa Store
//
//  Created by Niels on 19-05-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import UIKit

class InstallButton: UIButton {

    override internal var highlighted: Bool {
        didSet {
            if highlighted {
                backgroundColor = Color.sharedInstance.blueColor
                setTitleColor(UIColor(rgba: "#000000"), forState: .Highlighted)
                setTitleColor(UIColor(rgba: "#000000"), forState: .Normal)
                if #available(iOS 9.0, *) {
                    setTitleColor(UIColor(rgba: "#000000"), forState: .Focused)
                }
                titleLabel?.opaque = false
                titleLabel?.alpha = 1.0
            } else {
                backgroundColor = UIColor.clearColor()
                setTitleColor(Color.sharedInstance.blueColor, forState: .Normal)
                setTitleColor(Color.sharedInstance.blueColor, forState: .Highlighted)
            }
        }

    }
    
    
}
