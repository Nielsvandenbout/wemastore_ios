//
//  AppDelegate.swift
//  WeMa Store
//
//  Created by Niels on 13-05-16.
//  Copyright © 2016 Niels. All rights reserved.
//

import UIKit
import AVFoundation
import IQKeyboardManagerSwift
import Alamofire
import Fabric
import Crashlytics
//import Pushwoosh

var activityIndicator: CustomActivityIndicatorView = {
    let image: UIImage = UIImage(named: "downloadLoadingIndicator")!
    return CustomActivityIndicatorView(image: image)
}()


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, PushNotificationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        
        let newRootViewController = storyBoard.instantiateViewControllerWithIdentifier("tabVC") as! UITabBarController
        newRootViewController.selectedIndex = 1
        UINavigationBar.appearance().barStyle = UIBarStyle.Black
//        window?.tintColor = UIColor(red: 255/255, green: 88/255, blue: 0.0, alpha: 1.0)
        window?.tintColor = UIColor(colorLiteralRed: 19/256, green: 158/256, blue: 234/256, alpha: 1.0)
        UINavigationBar.appearance().setBackgroundImage(UIImage(), forBarMetrics: .Default)
        UINavigationBar.appearance().backgroundColor = UIColor.clearColor()
        UINavigationBar.appearance().translucent = true
        

        PushNotificationManager.pushManager().delegate = self
        PushNotificationManager.pushManager().handlePushReceived(launchOptions)
        PushNotificationManager.pushManager().sendAppOpen()
        
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName: UIFont.systemFontOfSize(18, weight: 0.3)]
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().canGoNext
        IQKeyboardManager.sharedManager().canGoPrevious
        let lagFreeField: UITextField = UITextField()
        self.window?.addSubview(lagFreeField)
        lagFreeField.becomeFirstResponder()
        lagFreeField.resignFirstResponder()
        lagFreeField.removeFromSuperview()
        
        Fabric.with([Crashlytics.self])

       
        let storeBuild = NSBundle.mainBundle().buildVersionNumber!
        let storeVersionInt = Int(storeBuild)!
        
        if Reachability.isConnectedToNetwork() {
            WeMaStoreAPI.sharedInstance.checkStoreVersion { (responseObject, error) in
                if responseObject != nil {
                    if Int((responseObject?.latestVersion?.version_no)!)! > storeVersionInt {
                        NSUserDefaults.standardUserDefaults().setObject(responseObject?.latestVersion?.download_url!, forKey: "download_url")
                        let updateViewController = storyBoard.instantiateViewControllerWithIdentifier("updateVC")
                        self.window?.rootViewController = updateViewController
                    } else {
                        if NSUserDefaults.standardUserDefaults().stringForKey("success") != nil || NSUserDefaults.standardUserDefaults().stringForKey("name") != nil || NSUserDefaults.standardUserDefaults().stringForKey("company_code") != nil {
                            let newRootViewController = storyBoard.instantiateViewControllerWithIdentifier("tabVC") as! UITabBarController
                            newRootViewController.selectedIndex = 1
                            self.window?.rootViewController = newRootViewController
                        } else {
                            let rootViewController = storyBoard.instantiateViewControllerWithIdentifier("loginVC")
                            self.window?.rootViewController = rootViewController
                            let newRootViewController = storyBoard.instantiateViewControllerWithIdentifier("tabVC") as! UITabBarController
                            newRootViewController.selectedIndex = 1
                        }
                    }
                } else {
                }
            }
        }
        return true
        
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        if let rootViewController = self.window?.rootViewController as? UITabBarController {
            if let openView = rootViewController.selectedViewController as? UINavigationController {
                if let VC = openView.topViewController as? StoreViewController {
                    VC.downloadData()
                } else if let VC = openView.topViewController as? UpdatesViewController {
                    VC.downloadData()
                } else if let VC = openView.topViewController as? TopRatedViewController {
                    VC.downloadData()
                }
            }
        }
    }

    func applicationDidBecomeActive(application: UIApplication) {
        let sess = AVAudioSession.sharedInstance()
        if sess.otherAudioPlaying {
            _ = try? sess.setCategory(AVAudioSessionCategoryAmbient, withOptions: []) //
            _ = try? sess.setActive(true, withOptions: [])
        }
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        PushNotificationManager.pushManager().handlePushRegistration(deviceToken)
        let pushId = PushNotificationManager.pushManager().getPushToken()
        debugPrint("pushID:\(pushId)")
        
        let name = NSUserDefaults.standardUserDefaults().stringForKey("name")
        
        if pushId != nil && name != nil {
            if name != "" {
                WeMaStoreAPI.sharedInstance.updatePushID(name!, push_id: pushId)
                debugPrint("registered with push ID")
            }
        }
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        PushNotificationManager.pushManager().handlePushRegistrationFailure(error)
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        PushNotificationManager.pushManager().handlePushReceived(userInfo)
    }
    
    func onPushAccepted(pushManager: PushNotificationManager!, withNotification pushNotification: [NSObject : AnyObject]!, onStart: Bool) {
        debugPrint("Push notification accepted: \(pushNotification)");
    }

}

